#Cel

Projekt mający na celu stworzyć podstawowe oddziaływanie fzyczne(odbicie) pomiędzy obiektami.

#Uruchomienie

Do uruchomienia potrzeba:

- biblioteki PhysX w wersji 3.2.5. [PhysX][id1]

[id1]: https://developer.nvidia.com/rdp/physx-325-apex-125-tools-downloads "PhysX"

- MS Visual Studio 2013 [Visual][id2]
[id2]: https://www.visualstudio.com/pl/vs/older-downloads/ "Visual Studio 2013"

- biblioteka OpenGL (do pobrania przez menadżera Visual Studio)

1. Zainstalować OpenGL.
2. Otworzyć projekt _ConsoleApplication1.vcxproj_
3. Podlinkować PhysX.
4. Uruchomić